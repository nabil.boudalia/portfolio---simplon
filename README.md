# PortFolio - Simplon

Ce template comporte une barre latérale fixe avec les sections de contenu du CV. 

L'ensemble des informations sont situées sur une seule page, orientée sur la gauche.

 Le but, avoir un rendu simple et élement qui lime le clique du visiteurs qui souhaite accèder à une information 



## Aperçu 

![](img/img-read-1.png)



## User Story

En tant qu'apprenant je réalise un portfolio afin de démontrer mes compténces pour sa propre réalisation ainsi que pour la présentation de mes autres projets.

En tant que profesionnel du recrutement, je souhaite avoir un apercu des comptences du candidat et en savoir plus sur le candidat. 



## Maquette



![](img/UntitledDiagram.svg)



Version mobile 

![](img/UntitledDiagram1.svg)



Avec Drow.io 

## Téléchargement et instalation

Pour utiliser ce teplate bootstap, il faut suivre cette demarche. 
* Aller sur bootstap: [Download the latest release on Start Bootstrap](https://startbootstrap.com/template-overviews/resume/)
* installer via npm: `npm i startbootstrap-resume`
* cloner le repo: `git clone https://github.com/BlackrockDigital/startbootstrap-resume.git`
* [Fork, Clone, or Download on GitHub](https://github.com/BlackrockDigital/startbootstrap-resume)

## Usage

### Utilisation de la template 

Après le téléchargement, il faut modifier le fichier HTML et CSS depuis notre IDE. Ce sont les 2 seule fichiers  qui necessite une préocupation. La prévisualisation se fait  en ouvrant le fichier `index.html` dans un navigateur soit via Apache soit en installant une extention visual code : live server

### Usage avancer

Après l’installation, lancer `npm install` puis` npm start` pour ouvrir un aperçu du modèle dans votre navigateur par défaut, surveiller les modifications apportées aux fichiers du modèle principal et recharger le navigateur en temps réel lorsque les modifications sont enregistrées. Vous pouvez voir le `gulpfile.js` pour voir quelles tâches sont incluses dans l'environnement dev.

#### Les tâches Gulp

- `gulp` c'est la tâche par défaut qui construit le tout le site
- `gulp watch` Le navigateur Sync ouvre le projet dans le navigateur par défaut. La chargement si des changement il y a, 
- `gulp css` compile les fichiers SCSS et CSS et réduit le CSS compliqué. 
- `gulp js` permet de réduire le fichier JS
- `gulp vendor`  copie les dépendances de node_modules dans le répertoire du fournisseur.

Il faut avoir npm installé globalement.

## Bug et solution du template

En cas de problement visiter:  [Open a new issue](https://github.com/BlackrockDigital/startbootstrap-resume/issues) (github) ou laisser un commentaire : [template overview page at Start Bootstrap](http://startbootstrap.com/template-overviews/resume/).

## Pourquoi ce choix de format 

L'ensemble des information est orientée à gauche.  Google à faconner la facon de voir un site. E

![](img/eyes-track.jpeg)

## Ergonomie du site 

Limiter le nombre de clique est effectivement necessaire mais d'un utlisateur à un autre la varience est très forte. A partir de 3 cliques le nombre d'abandon est marginal.

![](img/clicks-to-completion-testing-the-3-click-rule.jpg)

![](img/dissatisfaction-by-task-lenght-testing-the-3-click-rule-500x388.jpg)

source : https://www.usabilis.com/mythe-regle-3-clics/

## Pourquoi Bootstrap 

 Bootstrap est une bibliothèque open source de modèles et de thèmes Bootstrap gratuits. Tous les modèles et thèmes gratuits de Start Bootstrap sont publiés sous la licence MIT, ce qui signifie que vous pouvez les utiliser à n’importe quel objectif, même pour des projets commerciaux.

* https://startbootstrap.com
* https://twitter.com/SBootstrap

Bootstrap a été créé par et est maintenu pa **[David Miller](http://davidmiller.io/)**, propriètaire de [Blackrock Digital](http://blackrockdigital.io/).

* http://davidmiller.io
* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller



## Copyright et License

Copyright 2013-2019 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-resume/blob/gh-pages/LICENSE) license.

## Status

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-resume/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/startbootstrap-resume.svg)](https://www.npmjs.com/package/startbootstrap-resume)
[![Build Status](https://travis-ci.org/BlackrockDigital/startbootstrap-resume.svg?branch=master)](https://travis-ci.org/BlackrockDigital/startbootstrap-resume)
[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-resume/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-resume)
[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-resume/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-resume?type=dev)

## 